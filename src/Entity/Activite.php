<?php

namespace App\Entity;

use App\Repository\ActiviteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ActiviteRepository::class)
 */
class Activite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomActivite;

    /**
     * @ORM\ManyToOne(targetEntity=Seances::class, inversedBy="activites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $seance;

    /**
     * @ORM\ManyToMany(targetEntity=Eleve::class, inversedBy="activites")
     */
    private $lyceen;

    /**
     * @ORM\ManyToMany(targetEntity=Enseignants::class, inversedBy="activites")
     */
    private $enseignants;

    public function __construct()
    {
        $this->lyceen = new ArrayCollection();
        $this->enseignants = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getnomActivite(): ?string
    {
        return $this->nomActivite;
    }

    public function setnomActivite(string $nomActivite): self
    {
        $this->nomActivite = $nomActivite;

        return $this;
    }

    /**
     * @return Collection|Seances[]
     */
    public function getSeances(): Collection
    {
        return $this->seances;
    }

    public function addSeance(Seances $seance): self
    {
        if (!$this->seances->contains($seance)) {
            $this->seances[] = $seance;
            $seance->addActivite($this);
        }

        return $this;
    }

    public function removeSeance(Seances $seance): self
    {
        if ($this->seances->removeElement($seance)) {
            $seance->removeActivite($this);
        }

        return $this;
    }

    public function getSeance(): ?Seances
    {
        return $this->seance;
    }

    public function setSeance(?Seances $seance): self
    {
        $this->seance = $seance;

        return $this;
    }

    /**
     * @return Collection|Eleve[]
     */
    public function getLyceen(): Collection
    {
        return $this->lyceen;
    }

    public function addLyceen(Eleve $lyceen): self
    {
        if (!$this->lyceen->contains($lyceen)) {
            $this->lyceen[] = $lyceen;
        }

        return $this;
    }

    public function removeLyceen(Eleve $lyceen): self
    {
        $this->lyceen->removeElement($lyceen);

        return $this;
    }

    /**
     * @return Collection|Enseignants[]
     */
    public function getEnseignants(): Collection
    {
        return $this->enseignants;
    }

    public function addEnseignant(Enseignants $enseignant): self
    {
        if (!$this->enseignants->contains($enseignant)) {
            $this->enseignants[] = $enseignant;
        }

        return $this;
    }

    public function removeEnseignant(Enseignants $enseignant): self
    {
        $this->enseignants->removeElement($enseignant);

        return $this;
    }
    public function __toString(): string
    {
        return $this->nomActivite;
    } 
}
