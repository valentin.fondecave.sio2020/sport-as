<?php

namespace App\Entity;

use App\Repository\SeancesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SeancesRepository::class)
 */
class Seances
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity=Activite::class, mappedBy="seance")
     */
    private $activites;

    /**
     * @ORM\ManyToMany(targetEntity=Eleve::class, mappedBy="cours")
     */
    private $horaires;

    /**
     * @ORM\ManyToOne(targetEntity=Enseignants::class, inversedBy="seances")
     */
    private $enseignants;



    public function __construct()
    {
        $this->activites = new ArrayCollection();
        $this->horaires = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getSeance(): ?Activite
    {
        return $this->seance;
    }

    public function setSeance(?Activite $seance): self
    {
        $this->seance = $seance;

        return $this;
    }

    /**
     * @return Collection|Eleve[]
     */
    public function getHoraire(): Collection
    {
        return $this->Horaire;
    }

    public function addHoraire(Eleve $horaire): self
    {
        if (!$this->Horaire->contains($horaire)) {
            $this->Horaire[] = $horaire;
            $horaire->setSeances($this);
        }

        return $this;
    }

    public function removeHoraire(Eleve $horaire): self
    {
        if ($this->Horaire->removeElement($horaire)) {
            // set the owning side to null (unless already changed)
            if ($horaire->getSeances() === $this) {
                $horaire->setSeances(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Activite[]
     */
    public function getActivites(): Collection
    {
        return $this->activites;
    }

    public function addActivite(Activite $activite): self
    {
        if (!$this->activites->contains($activite)) {
            $this->activites[] = $activite;
            $activite->setSeance($this);
        }

        return $this;
    }

    public function removeActivite(Activite $activite): self
    {
        if ($this->activites->removeElement($activite)) {
            // set the owning side to null (unless already changed)
            if ($activite->getSeance() === $this) {
                $activite->setSeance(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Eleve[]
     */
    public function getHoraires(): Collection
    {
        return $this->horaires;
    }

    public function getEnseignants(): ?Enseignants
    {
        return $this->enseignants;
    }

    public function setEnseignants(?Enseignants $enseignants): self
    {
        $this->enseignants = $enseignants;

        return $this;
    }
    public function __toString(): string
    {
        return $this->getCreatedAt()->format("Y-m-d");
    } 
}
