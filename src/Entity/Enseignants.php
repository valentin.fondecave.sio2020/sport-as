<?php

namespace App\Entity;

use App\Repository\EnseignantsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;

/**
 * @ORM\Entity(repositoryClass=EnseignantsRepository::class)
 * @UniqueEntity(
 * fields={"email", "username"},
 *      message="Ce mail ou ce nom d'utilisateur existe déjà. "
 * )
 */
class Enseignants implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Prof;

    /**
     * @ORM\OneToMany(targetEntity=Seances::class, mappedBy="enseignants")
     */
    private $seances;

    /**
     * @ORM\ManyToMany(targetEntity=Activite::class, mappedBy="enseignants")
     */
    private $activites;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $username;

    /** 
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\Length(min=6,
     * max=50,
     * minMessage = "Your first name must be at least {{ limit }} characters long",
     * maxMessage ="Your first name cannot be longer than {{ limit }} characters" 
     * )
     */
    private $password;

    /**
     * @Assert\EqualTo(propertyPath="password", message = "La confirmation ne correspond pas"))
     */
    public $confirm_password;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    public function __construct()
    {
        $this->activites = new ArrayCollection();
        $this->seances = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProf(): ?string
    {
        return $this->Prof;
    }

    public function setProf(string $Prof): self
    {
        $this->Prof = $Prof;

        return $this;
    }

    /**
     * @return Collection|Seances[]
     */
    public function getSeances(): Collection
    {
        return $this->seances;
    }

    public function addSeance(Seances $seance): self
    {
        if (!$this->seances->contains($seance)) {
            $this->seances[] = $seance;
            $seance->setEnseignants($this);
        }

        return $this;
    }

    public function removeSeance(Seances $seance): self
    {
        if ($this->seances->removeElement($seance)) {
            // set the owning side to null (unless already changed)
            if ($seance->getEnseignants() === $this) {
                $seance->setEnseignants(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Activite[]
     */
    public function getActivites(): Collection
    {
        return $this->activites;
    }

    public function addActivite(Activite $activite): self
    {
        if (!$this->activites->contains($activite)) {
            $this->activites[] = $activite;
            $activite->addEnseignant($this);
        }

        return $this;
    }

    public function removeActivite(Activite $activite): self
    {
        if ($this->activites->removeElement($activite)) {
            $activite->removeEnseignant($this);
        }

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
    
    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt()
    {}

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {}
    
    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
     /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }
    
    public function __toString(): string
    {
        return $this->Prof;
    } 
}
