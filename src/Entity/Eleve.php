<?php

namespace App\Entity;

use App\Repository\EleveRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EleveRepository::class)
 */
class Eleve
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Eleve;

    /**
     * @ORM\ManyToMany(targetEntity=Activite::class, mappedBy="lyceen")
     */
    private $activites;

    /**
     * @ORM\ManyToMany(targetEntity=Seances::class, inversedBy="horaires")
     */
    private $cours;

    public function __construct()
    {
        $this->activites = new ArrayCollection();
        $this->cours = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEleve(): ?string
    {
        return $this->Eleve;
    }

    public function setEleve(string $Eleve): self
    {
        $this->Eleve = $Eleve;

        return $this;
    }

    /**
     * @return Collection|Activite[]
     */
    public function getActivites(): Collection
    {
        return $this->activites;
    }

    public function addActivite(Activite $activite): self
    {
        if (!$this->activites->contains($activite)) {
            $this->activites[] = $activite;
            $activite->addLyceen($this);
        }

        return $this;
    }

    public function removeActivite(Activite $activite): self
    {
        if ($this->activites->removeElement($activite)) {
            $activite->removeLyceen($this);
        }

        return $this;
    }

    /**
     * @return Collection|Seances[]
     */
    public function getCours(): Collection
    {
        return $this->cours;
    }

    public function addCour(Seances $cour): self
    {
        if (!$this->cours->contains($cour)) {
            $this->cours[] = $cour;
        }

        return $this;
    }

    public function removeCour(Seances $cour): self
    {
        $this->cours->removeElement($cour);

        return $this;
    }
    
    public function __toString(): string
    {
        return $this->Eleve;
    } 
}
