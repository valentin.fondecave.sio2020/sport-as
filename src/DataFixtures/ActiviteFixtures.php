<?php

namespace App\DataFixtures;

use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Activite;
use App\Entity\Enseignants;
use App\Entity\Seances;
use App\Entity\Eleve;

class ActiviteFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {      
    for($l=1;$l<=3;$l++) {
        $enseignants = new Enseignants();
        $enseignants -> setProf("P");
        $enseignants -> setEmail ("user@test.fr");
        $enseignants -> setUsername('user'.$l);
        $enseignants -> setPassword('totototo');           
        $manager->persist($enseignants);
        $seance = new Seances();
        $seance -> setCreatedAt(DateTime::createFromFormat('Y-m-d H:i:s',date('Y-m-d H:i:s')));
        $manager->persist($seance);
        $lyceen = new Eleve();
        $lyceen -> setEleve("Pascal");    
        $manager->persist($lyceen);
        $manager->flush();
        $activite = new Activite();
        $activite -> setnomActivite("Foot")
                  -> addEnseignant($enseignants)
                  -> setSeance($seance)
                  -> addLyceen($lyceen);

            $manager->persist($activite);
            $manager->flush();

            $lyceen -> addActivite($activite);
            $enseignants->addActivite($activite);

            $manager->flush();
    }
}
}