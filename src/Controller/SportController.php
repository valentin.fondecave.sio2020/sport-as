<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Activite;
use App\Repository\ActiviteRepository;
use App\Form\ActiviteType;

class SportController extends AbstractController
{
    /**
     * @Route("/sport", name="sport")
     */
    public function index(ActiviteRepository $repo): Response
    {
        //requete select * from annonce
        $activites = $repo->findAll();

        return $this->render('sport/index.html.twig', [
            'controller_name' => 'SportController',
            'activites' => $activites
        ]);
    }
    /**
     * @Route("/", name="home")
     */
    public function home() {
        return $this->render('sport/home.html.twig', [
            'title'=> "Bienvenue ici les amis !",
                  ]);
    }
    /**
     * @Route("/sport/new", name="sport_create")
     * @Route("/sport/{id}/edit", name="sport_edit")
     */
    public function create(Activite $activite = null, Request $request, EntityManagerInterface $manager){

        if(!$activite) {
            $activite = new Activite();
        }

        //$form = $this->createFormBuilder($activite)
        //                ->add('nomActivite')
        //                ->getForm();
        
        $form = $this->createForm(ActiviteType::class, $activite);
        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){ //vérif si envoyé et valide
            if ($activite->getId());//sauvegarde de l’article en bdd
            $manager->persist($activite);
            $manager->flush() ;
            //redirection vers la page de visualisation de l’article
            return $this->redirectToRoute('sport_show', ['id' =>$activite->getId()]) ;
        }


        return $this->render('sport/create.html.twig', [
            'formActivite' => $form->createView(),
            'editMode' => $activite->getId() !== null
        ]);

}
    /**
     * @Route("/sport/{id}", name="sport_show")
     */
    public function show(Activite $activite){
        
        return $this->render('sport/show.html.twig', [
            'activite' => $activite
        ]);
    }
   
}

