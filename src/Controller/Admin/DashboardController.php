<?php

namespace App\Controller\Admin;

use App\Entity\Activite;
use App\Entity\Enseignants;
use App\Entity\Eleve;
use App\Entity\Seances;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator; 
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator; 
use App\Controller\Admin\ActiviteCrudController; 
use App\Controller\Admin\EleveCrudController; 
use App\Controller\Admin\EnseignantsCrudController; 
use App\Controller\Admin\SeancesCrudController; 
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
     // redirect to some CRUD controller
            $routeBuilder = $this->get(AdminUrlGenerator::class);
        return $this->redirect($routeBuilder->setController(ActiviteCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(EleveCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(SeancesCrudController::class)->generateUrl());
        return $this->redirect($routeBuilder->setController(EnseignantsCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Sport');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Activite', 'fas fa-list', Activite::class);    
        yield MenuItem::linkToCrud('Seances', 'fas fa-list', Seances::class);    
        yield MenuItem::linkToCrud('Eleve', 'fas fa-list', Eleve::class);    
        yield MenuItem::linkToCrud('Enseignants', 'fas fa-list', Enseignants::class);    
 }
}