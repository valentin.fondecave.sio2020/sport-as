<?php

namespace App\Controller\Admin;

use App\Entity\Enseignants;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class EnseignantsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Enseignants::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('Prof'),
            TextField::new('email'),
            TextField::new('password'),
            TextField::new('username'),
            AssociationField::new('activites')
        ];
    }
}
