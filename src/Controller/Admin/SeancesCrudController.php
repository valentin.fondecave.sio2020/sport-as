<?php

namespace App\Controller\Admin;

use App\Entity\Seances;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class SeancesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Seances::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            DateTimeField::new('createdAt'),
            AssociationField::new('enseignants'),
            AssociationField::new('horaires')
        ];
    }
}
