# Sport AS

- Diagramme de classe
![Imageclasse](cas d'utilisation sport.PNG)

- Diagramme de cas d'utilisation
![Imagecas](use cas diagramme sport.PNG)

- Diagramme de séquence
![imageseq](diagramme de sequence 1.PNG)
![imageseq2](diagramme de sequence 2.PNG)

## SYMFONY 
 
 - A quoi sa sert?

    le site permet d'avoir une liste d'activité générer par des profs pouvant mettre les élèves de l'AS dans les activités de l'AS. Cette liste est consultable que par des profs.

![Image siteweb](siteweb.PNG)
 

 - Comment ca marche ?

    pour les profs ils peuvent créer leur compte et s'y connecter afin de créer leur activité en y inscrivant dans un formulaire: le nom de l'activité, le nom des élèves, l'heure et la date de la séance et le nom du prof, il peut modifier son activité en allant sur l'administrateur.

![Image prof](prof.PNG)

Et l'appel

![Image Appel](Appel.PNG)

Puis j'ai fais des déployements qui a mis le projet sur le serveur web du lycée.
![imageICCD](IC-CD.png)

![imagetest](test.png)

![imagedeploy](deploy_staging.png)

- Les démarches
    - j'ai commencer a créer l'accueil du site sous format html.twig en utilisant bootswatch pour le design ainsi que le menu. 
    - j'ai ensuite fait la partie controller qui possède toute les fonctions et les routes url du site internet.
    - j'ai crée la bdd
    - j'ai fait la liste des activités
    - création d'activité dans l'administrateur
    - inscription et connexion pour le prof
    - partie administration
    - les activités avec la liste des élèves pour les profs 
    - permission d'accès au page internet 
    - déploiement sur le serveur web
