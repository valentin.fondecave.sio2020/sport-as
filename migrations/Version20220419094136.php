<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220419094136 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE activite (id INT AUTO_INCREMENT NOT NULL, seance_id INT NOT NULL, nom_activite VARCHAR(255) NOT NULL, INDEX IDX_B8755515E3797A94 (seance_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE activite_eleve (activite_id INT NOT NULL, eleve_id INT NOT NULL, INDEX IDX_52DDC5FE9B0F88B1 (activite_id), INDEX IDX_52DDC5FEA6CC7B2 (eleve_id), PRIMARY KEY(activite_id, eleve_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE activite_enseignants (activite_id INT NOT NULL, enseignants_id INT NOT NULL, INDEX IDX_9A6D8E779B0F88B1 (activite_id), INDEX IDX_9A6D8E777CF12A69 (enseignants_id), PRIMARY KEY(activite_id, enseignants_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE eleve (id INT AUTO_INCREMENT NOT NULL, eleve VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE eleve_seances (eleve_id INT NOT NULL, seances_id INT NOT NULL, INDEX IDX_10D3A4EDA6CC7B2 (eleve_id), INDEX IDX_10D3A4ED10F09302 (seances_id), PRIMARY KEY(eleve_id, seances_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE enseignants (id INT AUTO_INCREMENT NOT NULL, prof VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', UNIQUE INDEX UNIQ_BA5EFB5AF85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE seances (id INT AUTO_INCREMENT NOT NULL, enseignants_id INT DEFAULT NULL, created_at DATETIME NOT NULL, INDEX IDX_FC699FF17CF12A69 (enseignants_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE activite ADD CONSTRAINT FK_B8755515E3797A94 FOREIGN KEY (seance_id) REFERENCES seances (id)');
        $this->addSql('ALTER TABLE activite_eleve ADD CONSTRAINT FK_52DDC5FE9B0F88B1 FOREIGN KEY (activite_id) REFERENCES activite (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE activite_eleve ADD CONSTRAINT FK_52DDC5FEA6CC7B2 FOREIGN KEY (eleve_id) REFERENCES eleve (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE activite_enseignants ADD CONSTRAINT FK_9A6D8E779B0F88B1 FOREIGN KEY (activite_id) REFERENCES activite (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE activite_enseignants ADD CONSTRAINT FK_9A6D8E777CF12A69 FOREIGN KEY (enseignants_id) REFERENCES enseignants (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE eleve_seances ADD CONSTRAINT FK_10D3A4EDA6CC7B2 FOREIGN KEY (eleve_id) REFERENCES eleve (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE eleve_seances ADD CONSTRAINT FK_10D3A4ED10F09302 FOREIGN KEY (seances_id) REFERENCES seances (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE seances ADD CONSTRAINT FK_FC699FF17CF12A69 FOREIGN KEY (enseignants_id) REFERENCES enseignants (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE activite_eleve DROP FOREIGN KEY FK_52DDC5FE9B0F88B1');
        $this->addSql('ALTER TABLE activite_enseignants DROP FOREIGN KEY FK_9A6D8E779B0F88B1');
        $this->addSql('ALTER TABLE activite_eleve DROP FOREIGN KEY FK_52DDC5FEA6CC7B2');
        $this->addSql('ALTER TABLE eleve_seances DROP FOREIGN KEY FK_10D3A4EDA6CC7B2');
        $this->addSql('ALTER TABLE activite_enseignants DROP FOREIGN KEY FK_9A6D8E777CF12A69');
        $this->addSql('ALTER TABLE seances DROP FOREIGN KEY FK_FC699FF17CF12A69');
        $this->addSql('ALTER TABLE activite DROP FOREIGN KEY FK_B8755515E3797A94');
        $this->addSql('ALTER TABLE eleve_seances DROP FOREIGN KEY FK_10D3A4ED10F09302');
        $this->addSql('DROP TABLE activite');
        $this->addSql('DROP TABLE activite_eleve');
        $this->addSql('DROP TABLE activite_enseignants');
        $this->addSql('DROP TABLE eleve');
        $this->addSql('DROP TABLE eleve_seances');
        $this->addSql('DROP TABLE enseignants');
        $this->addSql('DROP TABLE seances');
        $this->addSql('DROP TABLE user');
    }
}
