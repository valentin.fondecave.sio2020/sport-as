-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 15 déc. 2022 à 13:57
-- Version du serveur : 10.4.22-MariaDB
-- Version de PHP : 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `as`
--

-- --------------------------------------------------------

--
-- Structure de la table `activite`
--

CREATE TABLE `activite` (
  `id` int(11) NOT NULL,
  `seance_id` int(11) NOT NULL,
  `nom_activite` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `activite`
--

INSERT INTO `activite` (`id`, `seance_id`, `nom_activite`) VALUES
(14, 13, 'ski'),
(15, 16, 'golf'),
(16, 14, 'Foot'),
(17, 17, 'plongée'),
(18, 18, 'parapente'),
(19, 19, 'escalade'),
(20, 20, 'volley ball');

-- --------------------------------------------------------

--
-- Structure de la table `activite_eleve`
--

CREATE TABLE `activite_eleve` (
  `activite_id` int(11) NOT NULL,
  `eleve_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `activite_eleve`
--

INSERT INTO `activite_eleve` (`activite_id`, `eleve_id`) VALUES
(14, 25),
(14, 26),
(14, 28),
(14, 29),
(14, 30),
(15, 26),
(15, 28),
(15, 31),
(16, 26),
(16, 27),
(16, 29),
(16, 31),
(17, 27),
(17, 29),
(17, 30),
(17, 31),
(18, 26),
(18, 29),
(18, 30),
(19, 28),
(19, 30),
(20, 26),
(20, 27),
(20, 28),
(20, 31);

-- --------------------------------------------------------

--
-- Structure de la table `activite_enseignants`
--

CREATE TABLE `activite_enseignants` (
  `activite_id` int(11) NOT NULL,
  `enseignants_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `activite_enseignants`
--

INSERT INTO `activite_enseignants` (`activite_id`, `enseignants_id`) VALUES
(14, 24),
(15, 25),
(16, 24),
(17, 26),
(18, 25),
(19, 24),
(20, 26);

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20220419094136', '2022-04-19 11:42:26', 27311);

-- --------------------------------------------------------

--
-- Structure de la table `eleve`
--

CREATE TABLE `eleve` (
  `id` int(11) NOT NULL,
  `eleve` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `eleve`
--

INSERT INTO `eleve` (`id`, `eleve`) VALUES
(25, 'Lucie'),
(26, 'beltran salomé'),
(27, 'ainouz esteban'),
(28, 'herry céline'),
(29, 'beney-andreo nikita'),
(30, 'baus joan'),
(31, 'bourillon terry');

-- --------------------------------------------------------

--
-- Structure de la table `eleve_seances`
--

CREATE TABLE `eleve_seances` (
  `eleve_id` int(11) NOT NULL,
  `seances_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `eleve_seances`
--

INSERT INTO `eleve_seances` (`eleve_id`, `seances_id`) VALUES
(25, 13),
(26, 13),
(26, 14),
(26, 16),
(26, 18),
(26, 20),
(27, 14),
(27, 17),
(27, 20),
(28, 13),
(28, 16),
(28, 17),
(28, 20),
(29, 13),
(29, 14),
(29, 17),
(29, 18),
(30, 13),
(30, 17),
(30, 18),
(30, 19),
(31, 14),
(31, 16),
(31, 17),
(31, 20);

-- --------------------------------------------------------

--
-- Structure de la table `enseignants`
--

CREATE TABLE `enseignants` (
  `id` int(11) NOT NULL,
  `prof` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `enseignants`
--

INSERT INTO `enseignants` (`id`, `prof`, `email`, `username`, `password`, `roles`) VALUES
(21, 'P', 'Pejoan@Philipe.fr', 'Philippe', 'Excalibur', '[\"ROLE_ADMIN\"]'),
(22, 'P', 'user@test.fr', 'user2', 'totototo', '[]'),
(23, 'P', 'user@test.fr', 'user3', 'totototo', '[]'),
(24, 'PEJOAN Philippe', 'Pejoan.philippe@gmail.com', 'Pejoan Philippe', '$2y$13$Re31j9L3xvOaNmfSJUBVYePpMhifHrGN20Rv/gkdI.zsg5oKp0i3S', '[\"ROLE_ADMIN\"]'),
(25, 'Gauthier gaël', 'Gael.Gauthier@gmail.com', 'Gauthier gaël', '$2y$13$XzTYI2v06phsVsCKrjno1..R5t1om7j3xsUr8e.o4NaNdNITH7FJ.', '[]'),
(26, 'Bertand gaël', 'Gael.Bertand@gmail.com', 'Bertand gaël', '$2y$13$ah0ECxHqpHA7.uLMtT5SXetbc8WpeTuuP6Mq9mzuxUq6xYS9rvSg2', '[]');

-- --------------------------------------------------------

--
-- Structure de la table `seances`
--

CREATE TABLE `seances` (
  `id` int(11) NOT NULL,
  `enseignants_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `seances`
--

INSERT INTO `seances` (`id`, `enseignants_id`, `created_at`) VALUES
(13, 24, '2022-12-08 14:44:39'),
(14, 24, '2022-11-15 14:44:39'),
(16, 25, '2023-03-15 13:40:00'),
(17, 26, '2023-06-08 09:45:00'),
(18, 25, '2023-04-06 13:48:00'),
(19, 24, '2022-12-01 13:49:00'),
(20, 26, '2023-05-09 13:49:00');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `activite`
--
ALTER TABLE `activite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_B8755515E3797A94` (`seance_id`);

--
-- Index pour la table `activite_eleve`
--
ALTER TABLE `activite_eleve`
  ADD PRIMARY KEY (`activite_id`,`eleve_id`),
  ADD KEY `IDX_52DDC5FE9B0F88B1` (`activite_id`),
  ADD KEY `IDX_52DDC5FEA6CC7B2` (`eleve_id`);

--
-- Index pour la table `activite_enseignants`
--
ALTER TABLE `activite_enseignants`
  ADD PRIMARY KEY (`activite_id`,`enseignants_id`),
  ADD KEY `IDX_9A6D8E779B0F88B1` (`activite_id`),
  ADD KEY `IDX_9A6D8E777CF12A69` (`enseignants_id`);

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `eleve`
--
ALTER TABLE `eleve`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `eleve_seances`
--
ALTER TABLE `eleve_seances`
  ADD PRIMARY KEY (`eleve_id`,`seances_id`),
  ADD KEY `IDX_10D3A4EDA6CC7B2` (`eleve_id`),
  ADD KEY `IDX_10D3A4ED10F09302` (`seances_id`);

--
-- Index pour la table `enseignants`
--
ALTER TABLE `enseignants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_BA5EFB5AF85E0677` (`username`);

--
-- Index pour la table `seances`
--
ALTER TABLE `seances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_FC699FF17CF12A69` (`enseignants_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `activite`
--
ALTER TABLE `activite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `eleve`
--
ALTER TABLE `eleve`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT pour la table `enseignants`
--
ALTER TABLE `enseignants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT pour la table `seances`
--
ALTER TABLE `seances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `activite`
--
ALTER TABLE `activite`
  ADD CONSTRAINT `FK_B8755515E3797A94` FOREIGN KEY (`seance_id`) REFERENCES `seances` (`id`);

--
-- Contraintes pour la table `activite_eleve`
--
ALTER TABLE `activite_eleve`
  ADD CONSTRAINT `FK_52DDC5FE9B0F88B1` FOREIGN KEY (`activite_id`) REFERENCES `activite` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_52DDC5FEA6CC7B2` FOREIGN KEY (`eleve_id`) REFERENCES `eleve` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `activite_enseignants`
--
ALTER TABLE `activite_enseignants`
  ADD CONSTRAINT `FK_9A6D8E777CF12A69` FOREIGN KEY (`enseignants_id`) REFERENCES `enseignants` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_9A6D8E779B0F88B1` FOREIGN KEY (`activite_id`) REFERENCES `activite` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `eleve_seances`
--
ALTER TABLE `eleve_seances`
  ADD CONSTRAINT `FK_10D3A4ED10F09302` FOREIGN KEY (`seances_id`) REFERENCES `seances` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_10D3A4EDA6CC7B2` FOREIGN KEY (`eleve_id`) REFERENCES `eleve` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `seances`
--
ALTER TABLE `seances`
  ADD CONSTRAINT `FK_FC699FF17CF12A69` FOREIGN KEY (`enseignants_id`) REFERENCES `enseignants` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
