<?php

namespace App\Tests;

use App\Entity\Activite;
use PHPUnit\Framework\TestCase;

class ActiviteTest extends TestCase
{
    public function testAssertTrue(): void
    {
        $activite = new Activite();
        $activite->setnomActivite("nom de lactivite");

        $this->assertTrue($activite->getnomActivite()==='nom de lactivite',"Test du nom");
    }
    public function testAssertFalse(): void
    {
        $activite = new Activite();
        $this->assertFalse($activite->getnomActivite()==='nom lactivite',"Test du nom");  
    }
    public function testAssertEmpty(): void
    {
        $activite = new Activite();
        $this->assertEmpty($activite->getnomActivite(),"Test du nom");
    }
}
