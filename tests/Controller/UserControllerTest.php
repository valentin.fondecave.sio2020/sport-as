<?php

namespace App\Tests;

use App\Repository\EnseignantsRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    public function testVisitingWhileLoggedIn()
    {
    $client = static::createClient();
    $EnseignantsRepository = static::getContainer()->get(EnseignantsRepository::class);
    // retrieve the test user
    $testUser = $EnseignantsRepository->findOneByEmail('user@test.fr');
    // simulate $testUser being logged in
    $client->loginUser($testUser);
    // test e.g. the profile page
    $client->request('GET', '/security');
    $this->assertResponseIsSuccessful();
    $this->assertSelectorTextContains('h1', 'Hello SecurityController');
    }
}

