<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityControlleurTest extends WebTestCase
{
    public function testSecurity302(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/security');
        
        $this->assertResponseRedirects("/login", 302);
    }
}